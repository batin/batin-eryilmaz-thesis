module.exports = {
  ci: {
    collect: {
      settings: {chromeFlags: '--no-sandbox'},
      "numberOfRuns": 1
    },
    upload: {
      target: 'filesystem',
      reportFilenamePattern: "./reports/report.%%DATETIME%%.%%EXTENSION%%",
    },
  },
};